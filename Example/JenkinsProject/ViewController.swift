//
//  ViewController.swift
//  JenkinsProject
//
//  Created by d2rivendell on 12/01/2020.
//  Copyright (c) 2020 d2rivendell. All rights reserved.
//

import UIKit
import JenkinsProject
import SnapKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cView = CustomView()
        view.addSubview(cView)
        cView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(100)
            make.left.equalToSuperview().offset(50)
            make.size.equalTo(CGSize.init(width: 120, height: 80))
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

