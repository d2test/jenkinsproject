//
//  CustomView.swift
//  JenkinsProject
//
//  Created by leon on 2023/4/1.
//

import UIKit

public class CustomView: UIView {

    lazy var button = UIButton()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        button.backgroundColor = .red
        button.setTitle("Hello", for: .normal)
        addSubview(button)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        button.frame = bounds
    }

}
